//
//  ThirdViewController.swift
//  MRC_New
//
//  Created by Evan Gelfand on 1/8/17.
//  Copyright © 2017 STAX 3D Computer 1. All rights reserved.
//

import UIKit

class ThirdViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    @IBOutlet weak var textBox: UITextField!
    
    @IBOutlet weak var dropDown: UIPickerView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var emailDeveloperButton: UIButton!
    @IBOutlet weak var rateAppButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupButtonAndDescriptionFontSizes()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setupButtonAndDescriptionFontSizes() { //This setup will dynamically resize the button and label font sizes depending on the size of device.
        descriptionLabel.font = UIFont.systemFont(ofSize: self.view.frame.size.width * 0.03866666667)
        emailDeveloperButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: self.view.frame.size.width * 0.05333333333)
        rateAppButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: self.view.frame.size.width * 0.05333333333)
    }
    
    @IBAction func emailDeveloper(_ sender: Any) {
        
        if let url = URL(string: "mailto:evangelfand@gmail.com") {
            UIApplication.shared.open(url)
        }
        
    }
  

    var list = ["1", "2", "3"]
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
        
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        
        return list.count
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        self.view.endEditing(true)
        return list[row]
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.textBox.text = self.list[row]
        self.dropDown.isHidden = true
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.textBox {
            self.dropDown.isHidden = false
            //if you dont want the users to se the keyboard type:
            
            textField.endEditing(true)
        }
        
    }

}
