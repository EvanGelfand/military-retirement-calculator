//
//  FirstViewController.swift
//  MRC_New
//
//  Created by ESG
//
//  Needs: pickers, submit button for ease of coding

import UIKit

class FirstViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var lblYTR: UILabel!
    @IBOutlet weak var lblInputRecord: UILabel!
    @IBOutlet weak var lblCNY: UILabel!
    @IBOutlet weak var lblCNP: UILabel!
    @IBOutlet weak var lblPGR: UILabel!
    @IBOutlet weak var lblFNQY: UILabel!
    @IBOutlet weak var lblEPPY: UILabel!
    @IBOutlet weak var lblAPR: UILabel!
    @IBOutlet weak var lblTSPC: UILabel!
    @IBOutlet weak var lblTSPGR: UILabel!
    @IBOutlet weak var pointTextField: UITextField!
    @IBOutlet weak var currentQualityYears: UITextField!
    
    @IBOutlet weak var futureYearOfRetirement: UITextField!
    @IBOutlet weak var futurePayGrade: UITextField!
    @IBOutlet weak var futureYears: UITextField!
    @IBOutlet weak var futurePointsPerYear: UITextField!
    @IBOutlet weak var futurePayRaisePercent: UITextField!
    @IBOutlet weak var contributionPercentage: UITextField!
    @IBOutlet weak var tspGrowthPercentage: UITextField!
    
    @IBOutlet weak var oldMonthlyRetirement: UITextField!
    @IBOutlet weak var newMonthlyRetirement: UITextField!
    @IBOutlet weak var cashPayout: UITextField!
    @IBOutlet weak var noneCashOut: UITextField!
    
    @IBOutlet weak var imgGray: UIImageView!


    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    var tappedField: UITextField!
    var containerView: UIView!
    var selectedValue = "E5"
    var yearsArray = [1,2,3,4,4,6,6,8,8,10,10,12,12,14,14,16,16,18,18,20,20,22,22,24,24,26,26,28,28,30,30,32,32,34,34,36,36,38,38,40]
    let payArrayKeys = ["E1","E2","E3","E4","E5","E6","E7","E8", "E9", "O1", "O2", "O3", "O4", "O5", "O6"]
    
    let payArray = [
        [1566.90,1566.90,1566.90,1566.90,1566.90],
        [1756.50,1756.50,1756.50,1756.50,1756.50],
        [1847.10,1963.20,2082.00,2082.00,2082.00],
        [2046.00,2150.40,2267.10,2382.00,2483.40],
        [2231.40,2381.40,2496.60,2614.20,2797.80,2989.80,3147.60,3166.20],
        [2435.70,2680.20,2798.40,2913.60,3033.60,3303.30,3408.60,3612.30,3674.40,3719.70,3772.50],
        [2816.10,3073.50,3191.40,3347.10,3468.90,3678.00,3795.60,4004.70,4178.70,4297.50,4423.80],
        [4050.90,4050.90,4050.90,4050.90,4050.90,4050.90,4230.00,4341.00,4473.90,4618.20,4878.00,5009.40,5233.80,5358.00,5664.00,5664.00,5777.70],
        [4948.80,4948.80,4948.80,4948.80,4948.80,4948.80,4948.80,5060.70,5202.30,5368.20,5536.20,5804.70,6032.10,6270.90,6636.90,6636.90,6968.40,6968.40,7317.00,7317.00,7683.30],
        [2972.40,3093.90,3740.10,3740.10,3740.10,3740.10,3740.10,3740.10,3740.10,3740.10,3740.10],
        [3424.50,3900.30,4491.90,4643.70,4739.40,4739.40,4739.40,4739.40,4739.40,4739.40,4739.40],
        [3963.60,4492.80,4849.20,5287.20,5540.70,5818.80,5998.20,6293.70,6448.20,6448.20,6448.20],
        [4507.80,5218.20,5566.50,5643.90,5967.00,6313.80,6745.80,7081.50,7314.90,7449.30,7526.70],
        [5224.50,5885.70,6292.80,6369.60,6624.00,6776.10,7110.30,7356.00,7673.10,8158.50,8388.90],
        [6267.00,6885.30,7337.10,7337.10,7365.00,7680.90,7722.30,7722.30,8161.20,8937.00,992.70]
    ]
    
    var tspMatchArray = [1,3,5,7,8.5,10]

    override func viewDidLoad() {
        super.viewDidLoad()
        pointTextField.delegate = self
        currentQualityYears.delegate = self
        
        futurePayGrade.delegate = self
        futureYears.delegate = self
        futurePointsPerYear.delegate = self
        futurePayRaisePercent.delegate = self
        contributionPercentage.delegate = self
        tspGrowthPercentage.delegate = self
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action:#selector(FirstViewController.dismisskeyboard)))
        
        let font = UIFont.systemFont(ofSize: self.view.frame.size.width * 0.03866666667)
        lblCNP.font = font
        lblAPR.font = font
        lblCNY.font = font
        lblPGR.font = font
        lblEPPY.font = font
        lblTSPC.font = font
        lblTSPGR.font = font
        lblTSPC.font = font
        lblYTR.font = font
        pointTextField.font = font
        currentQualityYears.font = font
        futurePayGrade.font = font
        futureYears.font = font
        futurePointsPerYear.font = font
        futurePayRaisePercent.font = font
        contributionPercentage.font = font
        tspGrowthPercentage.font = font
        oldMonthlyRetirement.font = font
        newMonthlyRetirement.font = font
        cashPayout.font = font
        noneCashOut.font = font
        
        createBackground()
        addButtonAbovePayGradeAtRetirement() //Add button above PayGrade after retirement textfield to make it tappable to show the dropdown menu for E1, E2 and so on.
        NotificationCenter.default.addObserver(self, selector: #selector(self.saveDataBeforeExiting(sender:)), name: NSNotification.Name(rawValue: "SaveDataBeforeExit"), object: nil)
        sampleFunction(futureYears)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        addQuestionMark() // method for adding a question mark at the right side of the Expected Points per year text.
        displaySavedData()
        clearSavedData()
    }
    
    func createBackground() {
        var h:Float
        h = Float(tspGrowthPercentage.frame.origin.y + tspGrowthPercentage.frame.size.height) + Float(6.0) - Float(imgGray.frame.origin.y)
        let backgroundView = UIView.init()
        backgroundView.frame = CGRect(x: imgGray.frame.origin.x, y: 0, width: imgGray.frame.size.width, height: CGFloat(h) )
        backgroundView.backgroundColor = .black
        backgroundView.alpha = 0.05
        imgGray.addSubview(backgroundView)

        let whiteBG = UIView.init()
        whiteBG.frame = CGRect(x: 0, y: backgroundView.frame.height, width: imgGray.frame.size.width, height: 200)
        whiteBG.backgroundColor = .black
        whiteBG.alpha = 0.03
        imgGray.addSubview(whiteBG)
        
    }
    
    func displaySavedData() {
        if  (UserDefaults.standard.string(forKey: "currentQualityYears") != nil) {
            currentQualityYears.text = UserDefaults.standard.string(forKey: "currentQualityYears")
        }
        
        if  (UserDefaults.standard.string(forKey: "pointTextField") != nil) {
            pointTextField.text = UserDefaults.standard.string(forKey: "pointTextField")
        }
        
        if  (UserDefaults.standard.string(forKey: "futurePayGrade") != nil) {
            futurePayGrade.text = UserDefaults.standard.string(forKey: "futurePayGrade")
        }
        
        if  (UserDefaults.standard.string(forKey: "futureYears") != nil) {
            futureYears.text = UserDefaults.standard.string(forKey: "futureYears")
        }
        
        if  (UserDefaults.standard.string(forKey: "futurePointsPerYear") != nil) {
            futurePointsPerYear.text = UserDefaults.standard.string(forKey: "futurePointsPerYear")
        }
        
        if  (UserDefaults.standard.string(forKey: "futurePayRaisePercent") != nil) {
            futurePayRaisePercent.text = UserDefaults.standard.string(forKey: "futurePayRaisePercent")
        }
        
        if  (UserDefaults.standard.string(forKey: "contributionPercentage") != nil) {
            contributionPercentage.text = UserDefaults.standard.string(forKey: "contributionPercentage")
        }
        
        if  (UserDefaults.standard.string(forKey: "tspGrowthPercentage") != nil) {
            tspGrowthPercentage.text = UserDefaults.standard.string(forKey: "tspGrowthPercentage") 
        }
    }
    
    func clearSavedData() {
        UserDefaults.standard.set(nil, forKey: "currentQualityYears")
        UserDefaults.standard.set(nil, forKey: "pointTextField")
        UserDefaults.standard.set(nil, forKey: "futurePayGrade")
        UserDefaults.standard.set(nil, forKey: "futureYears")
        UserDefaults.standard.set(nil, forKey: "futurePointsPerYear")
        UserDefaults.standard.set(nil, forKey: "futurePayRaisePercent")
        UserDefaults.standard.set(nil, forKey: "tspGrowthPercentage")
        UserDefaults.standard.synchronize()
    }
    
    func saveDataBeforeExiting(sender: NSNotification) {
        UserDefaults.standard.set(currentQualityYears.text, forKey: "currentQualityYears")
        UserDefaults.standard.set(pointTextField.text, forKey: "pointTextField")
        UserDefaults.standard.set(futurePayGrade.text, forKey: "futurePayGrade")
        UserDefaults.standard.set(futureYears.text, forKey: "futureYears")
        UserDefaults.standard.set(futurePointsPerYear.text, forKey: "futurePointsPerYear")
        UserDefaults.standard.set(futurePayRaisePercent.text, forKey: "futurePayRaisePercent")
        UserDefaults.standard.set(tspGrowthPercentage.text, forKey: "tspGrowthPercentage")
        UserDefaults.standard.synchronize()
    }
    
    func addButtonAbovePayGradeAtRetirement() {
        let btnPayGrade = UIButton.init()
        btnPayGrade.frame = futurePayGrade.bounds
        btnPayGrade.backgroundColor = .clear
        btnPayGrade.addTarget(self, action: #selector(btnShowPickerView(sender:)), for: UIControlEvents.touchUpInside)
        futurePayGrade.addSubview(btnPayGrade)
    }
    
    func addQuestionMark() {
        let img = #imageLiteral(resourceName: "question_mark")
        let xpos = (self.view.frame.size.width - lblInputRecord.frame.size.height) - 18
        let qmarkButton = UIImageView.init(image:img)
        qmarkButton.frame = CGRect(x: xpos, y: lblInputRecord.frame.origin.y, width: lblInputRecord.frame.size.height, height: lblInputRecord.frame.size.height)
        qmarkButton.tag = 0
        qmarkButton.layer.cornerRadius = qmarkButton.frame.size.height/2
        qmarkButton.layer.masksToBounds = true
        qmarkButton.contentMode = .scaleAspectFit
        qmarkButton.isUserInteractionEnabled = true
        qmarkButton.addGestureRecognizer(UITapGestureRecognizer.init(target: self, action: #selector(self.showInfoTapped(sender:))))
        lblInputRecord.superview?.addSubview(qmarkButton)
        lblInputRecord.superview?.bringSubview(toFront: qmarkButton)
    }
    
    func showInfoTapped(sender: UITapGestureRecognizer) { //Displaying of popup information
        
        let popup = UIView.init(frame: (UIApplication.shared.keyWindow?.bounds)!)
        UIApplication.shared.keyWindow?.addSubview(popup)
        
        popup.backgroundColor = .clear
        
        let bgView = UIView.init()
        bgView.frame = (UIApplication.shared.keyWindow?.bounds)!
        bgView.backgroundColor = .black
        bgView.alpha = 0.55
        popup.addSubview(bgView)
        
        let style = NSMutableParagraphStyle.init()
        style.alignment = .left
        style.hyphenationFactor = 1
        
        let font  = UIFont.boldSystemFont(ofSize: UIScreen.main.bounds.size.width * 0.04066666667)
        let myMutableString = NSMutableAttributedString(
            string: GlobalVariables.sharedManager.info,
            attributes: [NSFontAttributeName:UIFont.systemFont(ofSize: UIScreen.main.bounds.size.width * 0.04066666667)])
        
        //Add more attributes here:
        myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 0,length: 24))
        myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 167,length: 16))
        myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 213,length: 16))
        myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 367,length: 12))
        //myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 451,length: 17))
        //myMutableString.addAttributes([NSFontAttributeName:font, NSParagraphStyleAttributeName:style], range: NSRange(location: 537,length: 17))
        
        
        
        let container = UIView.init(frame: CGRect(x: 30, y: 30, width: popup.frame.size.width - 60, height: popup.frame.size.height - 60))
        container.backgroundColor = .white
        container.layer.cornerRadius = 5
        container.layer.masksToBounds = true
        popup.addSubview(container)
        
        let label = UILabel.init(frame: CGRect(x: 15, y: 15, width: container.frame.size.width-30, height: container.frame.size.height))
        label.attributedText = myMutableString
        label.backgroundColor = .clear
        label.numberOfLines = 0
        container.addSubview(label)
        let rect  = myMutableString.boundingRect(with: CGSize(width:label.frame.size.width , height: CGFloat.greatestFiniteMagnitude), options: .usesLineFragmentOrigin, context: nil)
        label.frame = CGRect(x: 15, y: 15, width: label.frame.size.width, height: rect.size.height)
        
        let closeButton = UIButton.init(frame: CGRect(x: 0, y: label.frame.origin.y + label.frame.size.height + 20, width: container.frame.size.width, height: 50))
        closeButton.setTitle("CLOSE", for: .normal)
        closeButton.setTitleColor(.red, for: .normal)
        closeButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 20)
        closeButton.backgroundColor = .lightGray
        closeButton.addTarget(self, action: #selector(self.btnClosePopup(sender:)), for: .touchUpInside)
        container.addSubview(closeButton)
        
        let yoriginOfContainer:Float = (Float(popup.frame.size.height) - Float(closeButton.frame.origin.y + closeButton.frame.size.height))/Float(2.0)
        container.frame = CGRect(x: container.frame.origin.x, y: CGFloat(yoriginOfContainer), width: container.frame.size.width, height: closeButton.frame.origin.y + closeButton.frame.size.height)
    }
    
    func btnClosePopup(sender:UIButton) {
        sender.superview?.superview?.removeFromSuperview()
    }
    
    func  btnShowPickerView(sender: UIButton) { //The codes inside this method is for creating the dropdown view of the Pay Rate.
        self.view.endEditing(true)
        
        if  generateErrorMessages().characters.count == 0 {
            containerView = UIView.init()
            containerView.frame = (UIApplication.shared.keyWindow?.bounds)!
            containerView.alpha = 1
            UIApplication.shared.keyWindow?.addSubview(containerView)
            
            let selectedIndex : Int =  payArrayKeys.index(of: futurePayGrade.text!)!
            let pickerView = UIPickerView.init()
            pickerView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
            pickerView.backgroundColor = .white
            pickerView.delegate = self
            pickerView.dataSource = self
            pickerView.selectRow(selectedIndex, inComponent: 0, animated: true)
            containerView.addSubview(pickerView)
            
            let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(hideDropDown(sender:)))
            let clearColorView = UIView.init()
            clearColorView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
            clearColorView.alpha = 0.5
            clearColorView.backgroundColor = .darkGray
            clearColorView.addGestureRecognizer(tapRecognizer)
            containerView.addSubview(clearColorView)
            
            let buttonContainer = UIView.init()
            buttonContainer.frame = CGRect(x: 0, y: pickerView.frame.origin.y - (self.view.frame.size.height * 0.07496251874), width: self.view.frame.size.width, height:self.view.frame.size.height * 0.07496251874)
            buttonContainer.backgroundColor = .darkGray
            containerView.addSubview(buttonContainer)
            
            let btnCancel = UIButton.init(type: UIButtonType.roundedRect)
            btnCancel.frame = CGRect(x:5, y: 5, width: buttonContainer.frame.size.width/4, height: buttonContainer.frame.size.height - 10)
            btnCancel.setTitle("Cancel", for: UIControlState.normal)
            btnCancel.setTitleColor(UIColor.black, for: UIControlState.normal)
            btnCancel.backgroundColor = .white
            btnCancel.tag = 1
            btnCancel.layer.cornerRadius = 3
            btnCancel.layer.masksToBounds = true
            btnCancel.addTarget(self, action: #selector(btnDone(sender:)), for: UIControlEvents.touchUpInside)
            buttonContainer.addSubview(btnCancel)
            
            let btnDone = UIButton.init(type: UIButtonType.roundedRect)
            btnDone.frame = CGRect(x:btnCancel.frame.size.width * 3 - 5 , y: 4, width: btnCancel.frame.size.width, height: btnCancel.frame.size.height)
            btnDone.setTitle("Done", for: UIControlState.normal)
            btnDone.setTitleColor(UIColor.black, for: UIControlState.normal)
            btnDone.backgroundColor = .white
            btnDone.tag = 2
            btnDone.layer.cornerRadius = 3
            btnDone.layer.masksToBounds = true
            btnDone.addTarget(self, action: #selector(btnDone(sender:)), for: UIControlEvents.touchUpInside)
            buttonContainer.addSubview(btnDone)
        }
    }  //To show the codes, just tap (command + alt + right arrow)
    
    func btnDone(sender:UIButton) { //if the button tag is 2 which means the user has tapped the done button otherwise the user tapped the cancel button.
        futurePayGrade.resignFirstResponder()
        if sender.tag == 2 {
            futurePayGrade.text = selectedValue //assign new selected value for pay grade to the future text field
            sampleFunction(futurePayGrade) //called the sample function which do the calculation
            
        }
        UIView.animate(withDuration: 0.3, animations: { self.containerView.alpha = 0 }) { (Bool) in
            self.containerView.removeFromSuperview()
        }
    }
    
    func hideDropDown(sender:UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: { self.containerView.alpha = 0 }) { (Bool) in
            self.containerView.removeFromSuperview()
        }
    }
    
    func dismisskeyboard () {
        pointTextField.resignFirstResponder()
        currentQualityYears.resignFirstResponder()
        
        //futureYearOfRetirement.resignFirstResponder()
        futurePayGrade.resignFirstResponder()
        futureYears.resignFirstResponder()
        futurePointsPerYear.resignFirstResponder()
        futurePayRaisePercent.resignFirstResponder()
        contributionPercentage.resignFirstResponder()
        tspGrowthPercentage.resignFirstResponder()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func oldMonthlyRetirementPension(a: Double, b: Double, d: String, e: Double, f: Double, g: Double, h: Double, i: Double) {
        let totalPointsNumber  = Double(a + (e * f ))  //get total points
        var endPayAmount: Double
        let totalYears:Int = (Int)(round(b + e))// total years served
        let servedOver = yearsArray[Int(totalYears)]
        let index: Int  = payArrayKeys.index(of: d)!
        let payArrayCount:Int = payArray[index].count
    
        if (payArrayCount < servedOver ) {
            endPayAmount = payArray[index][(payArray[index].count)-1]
        }
        else {
            endPayAmount = payArray[index][servedOver]
        }
        
        print("The valueof payarrayCount is : \(payArrayCount) and served over : \(servedOver) andEndPayAmount: \(endPayAmount)")
        
        endPayAmount = endPayAmount * pow(1 + e, g / 100) //calculate final pay based off pay raise
        let h2 = tspMatchArray[Int(h)] //match array

        let finalMonthlyPay = endPayAmount / 30 * 4 //drill pay
        
        let equivalentYears = totalPointsNumber / 360
        
        let oldMonthlyRetirementPay = String(format: "%.2f", 2.5 * totalPointsNumber * endPayAmount * (1/100) / 360 )
        let newMonthlyRetirementPay = String(format: "%.2f", 2 * totalPointsNumber * endPayAmount * (1/100) / 360 )

        let annualInterestRate = Double (i * 0.01)
        
        let First = ( pow( Double (1 + ( annualInterestRate / 12)), e * 12) - 1)
        print (annualInterestRate)
        print (First)
        let Second = Double ( 12 / annualInterestRate )
        let Total = ( endPayAmount * h2 * 0.01 ) * First * Second
        //above based off of www.ajdesigner.com/phpinterest/interest_regular_deposits_p.php#ajscroll

        let cashPayoutPay = String ( format: "%.2f", Total )
    
        GlobalVariables.sharedManager.totalPointsSecond = String(format: "%.0f", totalPointsNumber)
        GlobalVariables.sharedManager.yearOfRetirementSecond = String(format: "%.0f", 2017 + e)
        GlobalVariables.sharedManager.finalMonthlyPaySecond = "$" + String(format: "%.2f", finalMonthlyPay)
        GlobalVariables.sharedManager.equivalentYearsSecond = String(format: "%.2f", equivalentYears)
        GlobalVariables.sharedManager.averageMonthlyTSPContributionSecond = "$" + String(format: "%.2f", endPayAmount * h * 0.01)
        
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        let number = NSNumber.init(value: Float(cashPayoutPay)!)
        
        oldMonthlyRetirement.text = "$" + oldMonthlyRetirementPay
        
        if (totalYears < 20 )  {
            oldMonthlyRetirement.text = "Need 20 Years"
            oldMonthlyRetirement.font = UIFont.systemFont(ofSize: 12)
        }
        if (totalPointsNumber < 1000 )  {
            oldMonthlyRetirement.text = "Need 1,000 Pts"
            oldMonthlyRetirement.font = UIFont.systemFont(ofSize: 12)
        }
        
        newMonthlyRetirement.text = "$" + newMonthlyRetirementPay
        cashPayout.text = "$" + formatter.string(from: number)!
        
        UserDefaults.standard.set(newMonthlyRetirementPay, forKey: "new_monthly_retirement_pay")
        UserDefaults.standard.synchronize()
        
    }
    
    @IBAction func sampleFunction(_ sender: UITextField) {

        let a: Double? = Double(pointTextField.text!)
        let b: Double? = Double(currentQualityYears.text!)
        //var c: Double? = Double(futureYearOfRetirement.text!
        let d: String? = String(futurePayGrade.text!)
        let e: Double? = Double(futureYears.text!)
        let f: Double? = Double(futurePointsPerYear.text!)
        let g: Double? = Double(futurePayRaisePercent.text!)
        let h: Double? = Double(contributionPercentage.text!)
        let i: Double? = Double(tspGrowthPercentage.text!)
        
        tappedField  = sender
        let errorMessage :String = generateErrorMessages()
        if  errorMessage.characters.count==0 {
            oldMonthlyRetirementPension(a: a!, b: b!, d: d!, e: e!, f: f!, g: g!, h: h!, i: i!)
        }else{
            let alertController  = UIAlertController.init(title: "Invalid Entry", message: errorMessage, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction.init(title: "OK", style: UIAlertActionStyle.destructive, handler: { (action: UIAlertAction) in
                sender.becomeFirstResponder()
            }))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func generateErrorMessages() -> String{
        var errorMessage:String = ""
        if  currentQualityYears.text?.characters.count == 0 {
            errorMessage = "Current number of years is required. Please try again"
        }else if Int(currentQualityYears.text!)! > 40 {
           errorMessage = "Current number of years must be less than or equal to 40.Please try again"
        
        }else if pointTextField.text?.characters.count == 0 {
           errorMessage = "Current number of points is required. Please try again"
        }else if Int(pointTextField.text!)! > 4000 {
            errorMessage = "Current number of points must be less than or equal to 4000. Please try again"
        
        }else if futureYears.text?.characters.count == 0 {
            errorMessage = "Future number of quality years is required. Please try again"
        }else if Int(futureYears.text!)! > 40 {
            errorMessage = "Future number of quality years must be less than or equal to 40. Please try again"
        
        }else if futurePointsPerYear.text?.characters.count == 0 {
            errorMessage = "Expected points per year is required. Please try again"
        }else if Int(futurePointsPerYear.text!)! > 365 {
            errorMessage = "Expected points per year must be less than or equal to 365. Please try again"
        
        }else if futurePayRaisePercent.text?.characters.count == 0 {
            errorMessage = "Annual pay raise (%) is required. Please try again"
        }else if Int(futurePayRaisePercent.text!)! > 10 {
            errorMessage = "Annual pay raise (%) must be less than or equal to 10. Please try again"
        
        }else if contributionPercentage.text?.characters.count == 0 {
            errorMessage = "TSP contribution (%) is required. Please try again"
        }else if Int(contributionPercentage.text!)! > 5 {
            errorMessage = "TSP contribution (%) must be less than or equal to 5. Please try again"
        
        }else if tspGrowthPercentage.text?.characters.count == 0 {
            errorMessage = "TSP growth rate (%) is required. Please try again"
        }else if Int(tspGrowthPercentage.text!)! > 10 {
            errorMessage = "TSP growth rate (%) must be less than or equal to 10. Please try again"
        }
        
        return errorMessage
    }
    

    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return payArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedValue = payArrayKeys[row]
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        
        let title = UILabel.init()
        title.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        title.text = "\(payArrayKeys[row])"
        title.textColor = .black
        title.backgroundColor = .lightGray
        title.textAlignment = .center

        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50;
    }
}

