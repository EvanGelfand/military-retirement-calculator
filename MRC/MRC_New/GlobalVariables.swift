//
//  GlobalVariables.swift
//  Military Retirement Calculator
//
//  Created by STAX 3D Computer 1 on 11/8/16.
//
//

import Foundation

class GlobalVariables {
    
    // These are the properties you can store in your singleton
    var totalPointsSecond: String = ""
    var oldMonthlyRetirement: String = ""
    var newMonthlyRetirement: String = ""
    var newCashPayout: String = ""
    
    var yearOfRetirementSecond: String = ""
    var finalMonthlyPaySecond: String = ""
    var equivalentYearsSecond: String = ""
    
    var averageMonthlyTSPContributionSecond: String = ""
    let infoTitleArray: [String] = ["Current Years: ",
                                    "Current Points:",
                                    "",
                                    "",
                                    "",
                                    "",
                                    ""]
   /* let infoArray: [String] = ["Current Years: Current number of qualifying years since enlisting or commissioning (can be taken from MyPay)",
                               "Current Points: Current number of retirement points (can be taken from MyPay)",
                               "",
                               "Future # of Qual years: Future number of qualifying years expected to stay in the military",
                               "Expected points per year: Average expected future retirement points per year. \nAverage: \nReserve – 75 \nNational Guard – 75 \nActive Duty – 365 \n\nFor deployment, divide",
                               "Annual pay raise: past five years averaged 1.7%, will vary based on future budgets",
                               "TSP contribution: Department of Defense matches 1%TSP growth: past five years averages 3.6%, will vary depending on US economy growth.",
                               "TSP growth: past five years averages 3.6%, will vary depending on US economy growth. "
                               ]*/
    
    /*let info =  "Current Years: Number of qualifying years since joining \n\nCurrent Points123: Current number of retirement points \n\nFuture # of Qual years: Future number of qualifying years expected to stay in the military \n\nExpected points per year: Average expected future retirement points per year. \nAverage: \nReserve – 75 \nNational Guard – 75 \nActive Duty – 365 \nFor deployment, divide \n\nAnnual pay raise: past five years averaged 1.7%, will vary based on future budgets \n\nTSP contribution: Department of Defense matches 1%TSP growth: past five years averages 3.6%, will vary depending on US economy growth."*/
    
     let info =  "Expected Points per Year \nReserve (TR): 75 \nNational Guard (NG): 75 \nActive Duty (AD): 365 \nFor NG or TR with AD days, this equals\n(Years * 75 + Active Days) / Years\n\nAnnual Pay Raise\n2012 to 2017 Average: 1.7% \n\nTSP contribution\nDoD matches the following:\nPersonal: 0%, DoD: 1%\nPersonal: 1%, DoD: 2%\nPersonal: 2%, DoD: 3%\nPersonal: 3%, DoD: 4%\nPersonal: 4%, DoD: 5%\n\nTSP Growth\n2012 to 2017 Average: 1.7%\n\n"
    

    
    
    let zipcodeArray: [String] = ["94016","94015","95012","94017","74130","94131","94146","94123","94999","12345","22342","64123","19201","90122"]
    
    // Here is how you would get to it without there being a global collision of variables.
    // , or in other words, it is a globally accessable parameter that is specific to the
    // class.
    class var sharedManager: GlobalVariables {
        struct Static {
            static let instance = GlobalVariables()
        }
        return Static.instance
    }
}
