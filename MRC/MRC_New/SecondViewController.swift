//
//  SecondViewController.swift
//  MRC_New
//
//  Created by STAX 3D Computer 1 on 11/28/16.
//  Copyright © 2016 STAX 3D Computer 1. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    @IBOutlet weak var totalPointsSecondView: UITextField!
    @IBOutlet weak var yearOfRetirement: UITextField!
    @IBOutlet weak var memberZipCode: UITextField!
    @IBOutlet weak var memberMonthlyCost: UITextField!
  //  @IBOutlet weak var equivalentYearsSecondView: UITextField!
    @IBOutlet weak var finalMonthlyPaySecondView: UITextField!
    @IBOutlet weak var yearOfRetirementSecondView: UITextField!
    @IBOutlet weak var averageMonthlyTSPContributionSecondView: UITextField!
    
    @IBOutlet weak var lblTP: UILabel!
    @IBOutlet weak var lblYOR: UILabel!
    @IBOutlet weak var lblMZP: UILabel!
    @IBOutlet weak var lblMMCOL: UILabel!
    @IBOutlet weak var lblTMTSPC: UILabel!
    @IBOutlet weak var lblFMTSPC: UILabel!
    @IBOutlet weak var lblFDP: UILabel!
    
    var urbanAreaArray = [AnyObject]()
    var containerView: UIView!
    var selectedValue: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if  urbanAreaArray.count == 0 {
            readUrbanAreaJson()
        }
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        memberZipCode.text = selectedValue
        totalPointsSecondView.text=GlobalVariables.sharedManager.totalPointsSecond
       // equivalentYearsSecondView.text=GlobalVariables.sharedManager.equivalentYearsSecond
        finalMonthlyPaySecondView.text=GlobalVariables.sharedManager.finalMonthlyPaySecond
        yearOfRetirementSecondView.text=GlobalVariables.sharedManager.yearOfRetirementSecond
        averageMonthlyTSPContributionSecondView.text=GlobalVariables.sharedManager.averageMonthlyTSPContributionSecond
        
        let font = UIFont.systemFont(ofSize: self.view.frame.size.width * 0.03866666667)
        
        totalPointsSecondView.font = font
        yearOfRetirement.font = font
        memberZipCode.font = font
        memberMonthlyCost.font = font
        finalMonthlyPaySecondView.font = font
        yearOfRetirementSecondView.font = font
        averageMonthlyTSPContributionSecondView.font = font
        
        lblTP.font = font
        lblYOR.font = font
        lblMZP.font = font
        lblMMCOL.font = font
        lblTMTSPC.font = font
        lblFMTSPC.font = font
        lblFDP.font = font
    
        addButtonAboveZipCode()
        
        
        
    }
    
    func readUrbanAreaJson() {
        do {
            if let file = Bundle.main.url(forResource: "urban_area", withExtension: "json") {
                let data = try Data(contentsOf: file)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                urbanAreaArray = json as! Array
                selectedValue = "Select urban area"
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }    }
    
    func addButtonAboveZipCode() {
        let btnPayGrade = UIButton.init()
        btnPayGrade.frame = memberZipCode.bounds
        btnPayGrade.backgroundColor = .clear
        btnPayGrade.addTarget(self, action: #selector(btnShowPickerView(sender:)), for: UIControlEvents.touchUpInside)
        memberZipCode.addSubview(btnPayGrade)
    }
    
    func getIndex() -> Int {
        var index:Int = 0
        for dic in urbanAreaArray {
            
            if  dic["Urban Area"] as? String == memberZipCode.text {
                return index
            }
            index += 1
        }
        return 0
    }
    
    func  btnShowPickerView(sender: UIButton) { //The codes inside this method is for creating the dropdown view of the Pay Rate.
        self.view.endEditing(true)
        containerView = UIView.init()
        containerView.frame = (UIApplication.shared.keyWindow?.bounds)!
        containerView.alpha = 1
        UIApplication.shared.keyWindow?.addSubview(containerView)
        
        let pickerView = UIPickerView.init()
        pickerView.frame = CGRect(x: 0, y: self.view.frame.size.height/2, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
        pickerView.backgroundColor = .white
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.selectRow(getIndex(), inComponent: 0, animated: true)
        containerView.addSubview(pickerView)
        
        let tapRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(hideDropDown(sender:)))
        let clearColorView = UIView.init()
        clearColorView.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height/2)
        clearColorView.alpha = 0.5
        clearColorView.backgroundColor = .darkGray
        clearColorView.addGestureRecognizer(tapRecognizer)
        containerView.addSubview(clearColorView)
        
        let buttonContainer = UIView.init()
        buttonContainer.frame = CGRect(x: 0, y: pickerView.frame.origin.y - (self.view.frame.size.height * 0.07496251874), width: self.view.frame.size.width, height:self.view.frame.size.height * 0.07496251874)
        buttonContainer.backgroundColor = .darkGray
        containerView.addSubview(buttonContainer)
        
        let btnCancel = UIButton.init(type: UIButtonType.roundedRect)
        btnCancel.frame = CGRect(x:5, y: 5, width: buttonContainer.frame.size.width/4, height: buttonContainer.frame.size.height - 10)
        btnCancel.setTitle("Cancel", for: UIControlState.normal)
        btnCancel.setTitleColor(UIColor.black, for: UIControlState.normal)
        btnCancel.backgroundColor = .white
        btnCancel.tag = 1
        btnCancel.layer.cornerRadius = 3
        btnCancel.layer.masksToBounds = true
        btnCancel.addTarget(self, action: #selector(btnDone(sender:)), for: UIControlEvents.touchUpInside)
        buttonContainer.addSubview(btnCancel)
        
        let btnDone = UIButton.init(type: UIButtonType.roundedRect)
        btnDone.frame = CGRect(x:btnCancel.frame.size.width * 3 - 5 , y: 4, width: btnCancel.frame.size.width, height: btnCancel.frame.size.height)
        btnDone.setTitle("Done", for: UIControlState.normal)
        btnDone.setTitleColor(UIColor.black, for: UIControlState.normal)
        btnDone.backgroundColor = .white
        btnDone.tag = 2
        btnDone.layer.cornerRadius = 3
        btnDone.layer.masksToBounds = true
        btnDone.addTarget(self, action: #selector(btnDone(sender:)), for: UIControlEvents.touchUpInside)
        buttonContainer.addSubview(btnDone)
    }  //To show the codes, just tap (command + alt + right arrow)
    
    func btnDone(sender:UIButton) { //if the button tag is 2 which means the user has tapped the done button otherwise the user tapped the cancel button.
        memberZipCode.resignFirstResponder()
        if sender.tag == 2 {
            selectedValue = memberZipCode.text!  //assign new selected value for pay grade to the future text field
        }else{
            memberZipCode.text = selectedValue
        }
        UIView.animate(withDuration: 0.3, animations: { self.containerView.alpha = 0 }) { (Bool) in
            self.containerView.removeFromSuperview()
        }
        
        var income:Float = 0
        income = urbanAreaArray[getIndex()]["Average Income"] as! Float
        memberMonthlyCost.text = "$\(income / UserDefaults.standard.float(forKey: "new_monthly_retirement_pay"))"
    }
    
    func hideDropDown(sender:UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3, animations: { self.containerView.alpha = 0 }) { (Bool) in
            self.containerView.removeFromSuperview()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
        
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return urbanAreaArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        memberZipCode.text  = (urbanAreaArray[row]["Urban Area"] as! String)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
    
        let string  = (urbanAreaArray[row]["Urban Area"] as! String)
        let title = UILabel.init()
        title.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 50)
        title.text = "\(string)"
        title.textColor = .black
        title.backgroundColor = .lightGray
        title.textAlignment = .center
        
        return title
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 50;
    }
    
}
